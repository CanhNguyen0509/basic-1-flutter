import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Rap chat',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primaryColor: Colors.black),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      initialIndex: 0,
      child: Scaffold(
        backgroundColor: Colors.black,
        drawer: new Drawer(
          child:new DrawerHeader(
          child: new Text("OPEN", style: TextStyle(
              fontSize: 30, color: Colors.white, fontWeight: FontWeight.bold)),
            decoration: new BoxDecoration(
              color: Colors.black),),
        ),
        appBar: AppBar(
          title: Text('Rap chat',
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.white)),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.share, size: 30, color: Colors.white),
                tooltip: "Share",
                onPressed: () => {}),
            IconButton(
                icon: Icon(Icons.search, size: 30, color: Colors.white),
                tooltip: "Search",
                onPressed: () => {}),
          ],
          bottom: TabBar(
            indicatorColor: Colors.white,
            tabs: [
              Tab(
                child: Text('Following', style: TextStyle(
                    fontSize: 15)
                ),),
              Tab(
                child: Text('Featured', style: TextStyle(
                  fontSize: 15),),),
              Tab(
                 child: Text( 'Beats',style: TextStyle(fontSize: 15),),),
            ],
          ),
        ),
        body: ListView(
          children: [
            Row(
              children: [
                CircleAvatar(radius: 17,
                backgroundColor: Color(0xffFDCF09),
                child: CircleAvatar(radius:15, backgroundImage: NetworkImage("https://s3.o7planning.com/images/boy-128.png"),
        ),),
                 Text("drippyyoc",
                    style: TextStyle(
                        fontSize: 15,
                        color: Colors.white),
                    textAlign: TextAlign.center),
          new IconButton(onPressed: () {}, icon: Icon(Icons.circle, color: Colors.grey, size: 8),
          ),
                Text("Follow", style: TextStyle(
                    fontSize: 15, color: Colors.white, fontWeight: FontWeight.bold),),
                Expanded(child: SizedBox()),
                IconButton(
                    onPressed: () {}, icon: Icon(Icons.more_vert_rounded, color: Colors.white)),
              ],
            ),

      Image.asset('assets/images/8.jpg'),],),
        floatingActionButton: FloatingActionButton(
            onPressed: () {},
            child: Icon(Icons.circle, size: 20, color: Colors.black),
            backgroundColor: Colors.white),
      ),
    );
  }
}
